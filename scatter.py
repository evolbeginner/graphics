#! /usr/local/bin python

from scipy.stats.stats import pearsonr
import numpy as np
import getopt
import re
import os
import sys
import matplotlib.pyplot as plt
import pylab

#############################################################
def read_list(list_file,sep,fields=[1,2]):
    lists=[[],[]]
    for line in open(list_file,'r'):
        line=line.rstrip('\n\r')
        f1,f2 = fields[0]-1,fields[1]-1
        line_array = re.split(sep,line)
        #line_array = map(lambda x:float(x), re.split(sep,line))
        values = float(line_array[f1]),float(line_array[f2])
        if isinstance(values[0],float) and isinstance(values[1],float):
            #if values[0] > 3:  continue
            lists[0].append(values[0])
            lists[1].append(values[1])
        else:
            continue
    return(lists)

def show_help():
    print "python " + os.path.basename(__file__) + " -l <input1> -l <input2>"
    print '''
Colors:
 'r' = red
 'g' = green
 'b' = blue
 'c' = cyan
 'm' = magenta
 'y' = yellow
 'k' = black
 'w' = white

 '-' = solid
 '--' = dashed
 ':' = dotted
 '-.' = dot-dashed
 '.' = points
 'o' = filled circles
 '^' = filled triangles
'''
    os.sys.exit()

#############################################################
inputs=[]
is_figure=True
fields=[]
separator="\t"
lists=[]
output=None
is_trendline=True
xlabel=None
ylabel=None
figure_args={}
figure_inches=[]
color='ob'

try:
    opts, args = getopt.getopt(
        sys.argv[1:],
        "hl:i:f:o:",
        ["list=","output=","field=","sep","xlabel=","ylabel=","no_figure=","no_trendline","dpi=","figure_inches=","color="],
    )
except getopt.GetoptError:
    print "illegal error!"
    show_help()
for op, value in opts:
    if op == '-l' or op == '-i' or op == '--list':
        inputs.append(value)
    elif op == '-o' or op == '--output':
        output=value
    elif op == '--no_figure':
        is_figure=False
    elif op == '-f' or op == '--field':
        for i in value.split(','):
            fields.append(int(i))
    elif op == '--sep':
        separator=value
    elif op == '--xlabel':
        xlabel=value
    elif op == '--ylabel':
        ylabel=value
    elif op == '--no_trendline':
        is_trendline=False
    elif op == '--dpi':
        figure_args['dpi']=int(value)
    elif op == '--figure_inches':
        figure_inches=map(lambda x: int(x), re.split(',',value))
    elif op == '--color':
        color=value
    elif op == '-h' or op == '--help':
        show_help()

if not fields:
    fields=[1,2]

#############################################################
for index, list_file in enumerate(inputs):
    lists.append(read_list(list_file,sep=separator,fields=fields))

for list in lists:
    x,y=list[0:2]
    print pearsonr(x,y)
    if is_figure:
        F = pylab.gcf()
        if not 'dpi' in figure_args:
            figure_args['dpi'] = F.get_dpi()
        if figure_inches:
            F.set_size_inches(figure_inches[0],figure_inches[1])
        # plot the data itself
        if xlabel:
            pylab.xlabel(xlabel)
        if ylabel:
            pylab.ylabel(ylabel)
        if is_trendline:
            pylab.plot(x,y,color)
            z = np.polyfit(x,y,1)
            p = np.poly1d(z)
            pylab.plot(x,p(x),"-r")
        else:
            plt.scatter(list[0], list[1])
        if output != None:
            pylab.savefig(output,dpi=figure_args['dpi'])
        else:
            pylab.show()

