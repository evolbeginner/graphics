#! /bin/env python

import random
import matplotlib.pyplot as plt
import re
import getopt
import os
import sys

#########################################################################
def isNumeric(val):
    try: 
        float(val);
        return True
    except(ValueError): 
        return False

def show_help():
    os.sys.exit()

def read_inputs(input,sep="\t",field=1,max=None,min=None):
    result=[]
    fp=open(input, "r");
    for line in fp:
        line=line.rstrip('\n\r')
        number = line.split(sep)[field-1]
        if isNumeric(number):
            number=float(number)
            if max:
                if number > max:   continue
            if min:
                if number < min:    continue
            result.append(number)
    return result

def get_weights(hist_info,is_fraction=True):
    weights=[]
    for i in hist_info:
        length_of_array = len(i)
        if is_fraction:
            denominator = float(length_of_array)
        else:
            denominator = 1.0
        a = [1.0/denominator] * length_of_array
        weights.append(a)
    return(weights)


#########################################################################
inputs=[]
colors=[]
hist_info=[]
output=None
outfmt="PDF"
sep="\t"
field=1
add_args=None
binsize=1
histtype="bar" # step, stepfilled
is_fraction=True
max,min = None,None
colors=["blue","red","green","yellow","purple","orange","cyan","grey","pink"]
xlabel=None
ylabel=None
is_stacked=False
is_cumulative=False


try:
    opts, args = getopt.getopt(
        sys.argv[1:],
        "hi:o:f:", 
        ["help","in=","output=","outfmt","field=","sep=","binsize=","binSize=","max=","min=","color=","frequency","histtype=","xlabel=","ylabel=","args=","stack","stacked","cumulative"])
except getopt.GetoptError:
    print "Illegal params!"
    show_help()
for op, value in opts:
    if op == '-i' or op == '--in':
        inputs.append(value)
    elif op == '-o' or op == '--out':
        output=value
    elif op == '--outfmt':
        outfmt=value
    elif op == '-h' or op == '--help':
        show_help()
    elif op == '--sep':
        sep=value
    elif op == '-f' or op == '--field':
        field=int(value)
    elif re.search(r'^--bin[sS]ize$',op):
        binsize=float(value)
    elif op == '--max':
        max=float(value)
    elif op == '--min':
        min=float(value)
    elif op == '--color':
        colors.insert(0,value)
    elif op == '--frequency':
        is_fraction=False
    elif op == '--histtype':
        histtype=value
    elif op == '--xlabel':
        xlabel=value
    elif op == '--ylabel':
        ylabel=value
    elif op == '--args':
        add_args=value
    elif op == "--stack" or op == "--stacked":
        is_stacked=True
    elif op == "--cumulative":
        is_cumulative=True

inputs=map(lambda x: os.path.expanduser(x), inputs)

#########################################################################
for input in inputs:
    hist_info.append(read_inputs(input,sep,field,max,min))

weights = get_weights(hist_info,is_fraction)
xbins = reduce (lambda x,y: x+y, map (lambda x:len(x), hist_info))/len(hist_info)/binsize
plt.hist(hist_info, bins=xbins, color=colors[0:len(inputs)], histtype=histtype, alpha=0.5, weights=weights, stacked=is_stacked, cumulative=is_cumulative)

if xlabel:
    plt.xlabel(xlabel)
if ylabel:
    plt.ylabel(ylabel)


if output:
    #plt.use(outfmt)
    plt.savefig(output)
else:
    plt.show()

