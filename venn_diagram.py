#! /bin/env python

from matplotlib import pyplot as plt
import numpy as np
from matplotlib_venn import venn3, venn3_circles
import sys
import getopt
import itertools
import os

#######################################################################
def read_infiles(infiles):
    containers=[]
    for infile in infiles:
        lines = []
        for line in open(infile, 'r'):
            line = line.rstrip('\r\n')
            lines.append(line)
        containers.append(set(lines))
    return containers


def generate_venn_relationships(containers):
    venn_array=[]
    all_items = reduce(lambda x,y: x | y, containers)
    for index,value in enumerate(containers):
        venn_array.append(len(value-(all_items-value)))

    counter = 2
    while counter <= len(containers):
        for combination in list(itertools.combinations(containers, counter)):
            intersect = reduce(lambda x,y: set(x) & set(y), combination)
            venn_array.append(len(intersect))
        counter += 1
    return(venn_array)


#######################################################################
infiles=[]
containers=[]

try:
    opts,args = getopt.getopt(
        sys.argv[1:],
        "hi:",
        ["in=",],
    )
except getopt.GetoptError:
    print "illegal error!"
    show_help()

for op, value in opts:
    if op == '-i' or op == '--in':
        infiles.append(os.path.expanduser(value))
        

#######################################################################
containers = read_infiles(infiles)
venn_array = generate_venn_relationships(containers)

plt.figure(figsize=(4,4))

venn_array=[1,2,3,4,5,6,7,8,9,10,11,12]
print venn_array

v = venn3(subsets=venn_array)
v.get_patch_by_id('100').set_alpha(1.0)

#v = venn3(subsets=(5, 1, 1, 1, 1, 1, 10), set_labels = ('A', 'B', 'C'))
#v.get_patch_by_id('100').set_alpha(1.0)

#v.get_patch_by_id('100').set_color('white')
#v.get_label_by_id('100').set_text('Unknown')
#v.get_label_by_id('A').set_text('Set "A"')
#c = venn3_circles(subsets=(1, 1, 1, 1, 1, 1, 1), linestyle='dashed')
#c = venn3_circles(subsets=(1, 2, 3, 4, 5, 6, 7), linestyle='dashed')
#c[0].set_lw(1.0)
#c[0].set_ls('dotted')
#plt.annotate('Unknown set', xy=v.get_label_by_id('100').get_position() - np.array([0, 0.05]), xytext=(-70,-70),
#             ha='center', textcoords='offset points', bbox=dict(boxstyle='round,pad=0.5', fc='gray', alpha=0.1),
#             arrowprops=dict(arrowstyle='->', connectionstyle='arc3,rad=0.5',color='gray'))

plt.title("Sample Venn diagram")
plt.show()

