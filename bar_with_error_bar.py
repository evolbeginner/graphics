#! /usr/local/bin/python

"""
Bar chart demo with pairs of bars grouped for easy comparison.
"""

import sys
import os
import re
import getopt
import numpy as np
import matplotlib.pyplot as plt
import random
import copy
import math


############################################################
def show_help():
    basename = os.path.basename()
    print "Usage: python " + basename + " <-i|--in|--input=> <-o|--ou|--output=> [-n_groups]"
    sys.exit()

def get_data_from_inputs(inputs, input_sep="\t"):
    input_info = {}
    for index1, input in enumerate(inputs):
        with open(input) as fh:
            input_info[index1]={}
            for line in fh:
                line.rstrip('\n\r')
                for index2, ele in enumerate(re.split(input_sep, line)):
                    if not index2 in input_info[index1]:
                        input_info[index1][index2] = []
                    if not re.search('\d', ele):
                        continue
                    input_info[index1][index2].append(float(ele))
    return(input_info)


############################################################
inputs = []
output = None
n_groups = None
input_info = {}
mean = {}
std = {}
xlabel, ylabel = None, None
title = None
xticks = []
labels = []
colors = []
ylim_min = None
ylim_max = None

bar_width = 0.35
opacity = 1.0
error_config = {'ecolor': '0.3'}
spacer_width = 0
init_start = 0
is_std = True
is_se = False # is standard error shown instead of std?
is_stack = False


try:
    opts,args = getopt.getopt(
        sys.argv[1:],
        "i:o:h",
        ["in=", "input=", "out=", "output=", "no_groups=", "xlabel=", "ylabel=", "label=", "title=", "xtick=", "bar_width=", "ymin=", "ylim_min=", "ymax=", "ylim_max=", "spacer=", "spacer_width=", "init_start=", "init=", "color=", "opacity=", "no_std", "se", "stack", "help"]
    )
except getopt.GetoptError:
    print "Illegal Params! Exiting ......"
    sys.exit()


for op, value in opts:
    if op == "-i" or op == "--in" or op == "--input":
        for i in re.split(',', value):
            inputs.append(i)
    elif op == "-o" or op == "--out" or op == "--output":
        output = value
    elif op == "--no_groups":
        n_groups = int(value)
    elif op == "--xlabel":
        xlabel = value
    elif op == "--ylabel":
        ylabel = value
    elif op == "--label":
        for i in re.split(',', value):
            labels.append(i)
    elif op == "--title":
        title = value
    elif op == "--xtick":
        for i in value.split(','):
            xticks.append(i)
    elif op == "--bar_width":
        bar_width=float(value)
    elif op == "--ymin" or op == "--ylim_min":
        ylim_min = float(value)
    elif op == "--ymax" or op == "--ylim_max":
        ylim_max = float(value)
    elif op == "--spacer" or op == "--spacer_width":
        spacer_width = float(value)
    elif op == "--init" or op == "--init_start":
        init_start = float(value)
    elif op == "--color":
        for color in value.split(','):
            colors.append(color)
    elif op == "--opacity":
        opacity = int(value)
    elif op == "--no_std":
        is_std = False
    elif op == "--se":
        is_se = True
    elif op == "--stack":
        is_stack = True
    elif op == "-h" or op == "--help":
        show_help()
        
if not colors:
    colors = ['red', 'blue', 'green', 'yellow', 'cyan', 'orange', 'purple', 'grey', 'black', 'pink', 'cyan']


############################################################
input_info = get_data_from_inputs(inputs)

for index1 in sorted(input_info.keys()):
    mean[index1] = {}
    std[index1] = {}
    v = input_info[index1]
    n_groups = len(input_info[0].keys())
    for index2 in sorted(v.keys()):
        a = np.array(v[index2]) 
        mean[index1][index2] = np.mean(a)
        std[index1][index2] = np.std(a)
        if is_se:
            std[index1][index2] = std[index1][index2]/math.sqrt(len(a))



############################################################
fig, ax = plt.subplots()

index = np.arange(n_groups)

rects = {}


pre_means_0 = map(lambda x: 0, range(0,n_groups))
pre_means = copy.deepcopy(pre_means_0)
for index2 in range(0, len(inputs)):
    means = map(lambda x: mean[index2][x], range(0,n_groups))
    if is_std:
        stds = map(lambda x: std[index2][x], range(0,n_groups))
    else:
        stds = map(lambda x: 0, range(0,n_groups))
    if is_stack:
        index_distance = 0
        bottom_means = pre_means
    else:
        index_distance = index2
        bottom_means = pre_means_0

    print is_stack
    rects[index2] = plt.bar( init_start+index+(bar_width+spacer_width)*index_distance, means, bar_width,
                             alpha=opacity,
                             color=[colors[index2]],
                             bottom=bottom_means,
                             yerr=stds,
                             error_kw=error_config,
                             label=labels[index2],
                             )
    print "mean:\t"
    print means
    print "stds:\t"
    print stds
    pre_means = means


if xlabel:
    plt.xlabel(xlabel)
if ylabel:
    plt.ylabel(ylabel)
if title:
    plt.title(title)
if xticks:
    plt.xticks(index + bar_width, xticks)
plt.legend()

plt.tight_layout()
ax.set_xlim(-bar_width,2.1+bar_width)
if ylim_min != None and ylim_max != None:
    ax.set_ylim([ylim_min,ylim_max])
elif ylim_min != None:
    ax.set_ylim([ylim_min,])

ax.set_xlim(-bar_width,len(index)+bar_width)


##############################################################################
if output:
    plt.savefig(output)
else:
    plt.show()



