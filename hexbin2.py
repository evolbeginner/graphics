#! /usr/local/bin/python

import numpy as np
import matplotlib.pyplot as plt
import sys
import getopt
import re

######################################################################
def read_input(input):
    x_list = []
    y_list = []
    fp=open(input, "r")
    for line in fp:
        if line.startswith('#'):   continue
        line = line.rstrip('\n\r')
        line_array = re.split("\s+", line)
        x_list.append(float(line_array[0]))
        y_list.append(float(line_array[1]))
    x_np_array = np.array(x_list)
    y_np_array = np.array(y_list)
    return(x_np_array, y_np_array)


######################################################################
input = None 
gridsize = 20

try:
    opts, args = getopt.getopt(
        sys.argv[1:],
        "hi:o:", 
        ["help","in=","grid_size=","gridsize=","output="])
except getopt.GetoptError:
    print "Illegal params!"
    show_help()
for op, value in opts:
    if op == '-i' or op == '--in':
        input = value
    elif op == "--gridsize" or op == "--grid_size":
        gridsize = float(value)


######################################################################
x, y = read_input(input)
xmin = x.min()
xmax = x.max()
ymin = y.min()
ymax = y.max()

plt.subplots_adjust(hspace=0.5)
#plt.subplot(121)
plt.hexbin(x,y, cmap=plt.cm.YlOrRd_r, gridsize = gridsize)
plt.axis([xmin, xmax, ymin, ymax])
plt.title("Hexagon binning")
cb = plt.colorbar()
cb.set_label('counts')

'''
plt.subplot(122)
plt.hexbin(x,y,bins='log', cmap=plt.cm.YlOrRd_r)
plt.axis([xmin, xmax, ymin, ymax])
plt.title("With a log color scale")
cb = plt.colorbar()
cb.set_label('log10(N)')
'''

plt.show()


