#! /bin/env python

import numpy as np
import matplotlib.pyplot as plt
import re
import sys
import getopt
import numpy as np
import os


########################################################################
def read_input(input,fields,separator="\t",is_std=False):
    means=None
    std=0
    hash={}
    for line in open(input,'r'):
        line=line.rstrip('\r\n')
        line_array = map(lambda x: float(x),re.split(separator,line))
        for index,value in enumerate(line_array):
            if not index+1 in fields:
                continue
            if not index in hash:
                hash[index]=[]
            else:
                hash[index].append(value)
    for key,value in hash.iteritems():
        mean=np.mean(value)
        if is_std:
            std=np.std(value, axis=0)
        else:
            std=0
    return mean,std

def show_help():
    print "python " + os.path.basename(__file__) + " <-i> [-i] ... [-i] [Options]"
    os.sys.exit()


########################################################################
inputs=[]
inputs_array=[]
output=None
indir=None
bar_info={}
separator="\t"
legends=[]
fields=[]
width = 0.35 # the width of the bars
is_std=False
colors=["blue","red","green","yellow","purple","orange","cyan","grey","pink"]
is_diff_color_within_group=False
xTickMarks=[]
ylabel=None
title=None
ind=1

try:
    opts,args = getopt.getopt(
        sys.argv[1:],
        "hi:l:o:f:",
        ["indir=","list=","output=","legend=","width=","std","ia=","input_array=",
        "diff_color","xTickMarks=","ylabel=","title="],
    )
except getopt.GetoptError:
    print "illegal error!"
    show_help()

for op, value in opts:
    if op == '-l' or op == '-i' or op == '--list':
        inputs.append(value)
    if op == '-o' or op == '--output':
        output=value
    elif op == '--indir':
        indir=value
        for file in sorted(os.listdir(indir)):
            inputs.append(os.path.join(indir,file))
    elif op == '--ia' or op == '--input_array':
        tmp_array=[]
        for i in re.split(',',value):
            tmp_array.append(i)
        inputs_array.append(tmp_array)
    elif op == "--legend":
        for i in re.split(',',value):
            legends.append(i)
    elif op == "-f" or op == "--field":
        for i in re.split(',',value):
            fields.append(float(i))
    elif op == '--width':
        width=float(value)
    elif op == '--color':
        for i in re.split(',',value):
            colors.insert(0,i)
    elif op == '--std':
        is_std=True
    elif op == '--diff_color':
        is_diff_color_within_group=True
    elif op == '--xTickMarks':
        for i in re.split(',',value):
            xTickMarks.append(i)
    elif op == '--ylabel':
        ylabel=value
    elif op == '--title':
        title=value
    elif op == '-h' or op == "--help":
        show_help()


########################################################################
rects_0=[]
fig = plt.figure()
ax = fig.add_subplot(111)
start=-0.5*len(inputs_array)*width

for index1,inputs in enumerate(inputs_array): # inputs: array of inputs
    means=[]
    stds=[]
    for index2,input in enumerate(inputs):
        mean,std = read_input(input,separator="\t",fields=fields,is_std=is_std)
        means.append(mean)
        stds.append(std)
    n_groups = len(means)
    ind = np.arange(n_groups)
    start+=width
    if is_diff_color_within_group:
        color_array=colors[0:len(inputs)] 
    else:
        color_array=[colors[index1]] * len(inputs)
    rects = ax.bar(ind+start, means, width,
                    color=color_array,
                    yerr=stds,
                    error_kw=dict(elinewidth=2,ecolor='black'),
                    #align='center',
                    )
    rects_0.append(rects[0])

ax.set_xlim(-width,len(ind)+width)
'''
min,max=min(menMeans),max(menMeans)
print min
#ax.set_ylim(max=max+0.2*abs(max))
'''
if ylabel:
    ax.set_ylabel(ylabel)
if title:
    ax.set_title(title)
if not xTickMarks:
    xTickMarks = ['Group'+str(i) for i in range(1,6)]
ax.set_xticks(ind+width)
xtickNames = ax.set_xticklabels(xTickMarks)
#plt.setp(xtickNames, rotation=45, fontsize=10)

if legends:
    ax.legend(rects_0, legends)

if output:
    plt.savefig(output,dpi=(500))
else:
    plt.show()


